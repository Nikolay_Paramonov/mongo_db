from euler_function import *
from pymongo import MongoClient

cluster = MongoClient("mongodb://127.0.0.1:27017")
db = cluster["Euler_Tasks"]
collection = db["Tasks"]
print("The connection was successful")
collection.drop()
# Добавляем документ в бд
task1 = ({"_id": 1, "answer": euler_1()}, {"_id": 2, "answer": euler_2()},
         {"_id": 5, "answer": euler_5()}, {"_id": 6, "answer": euler_6()},
         {"_id": 9, "answer": euler_9()}, {"_id": 12, "answer": euler_12()},
         {"_id": 20, "answer": euler_20()})
collection.insert_many(task1)
