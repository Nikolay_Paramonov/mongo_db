from flask import Flask, request
from pymongo import MongoClient
app = Flask(__name__)


@app.route('/<data_base>/<collection>/')
def query(data_base, collection):
    address = "mongodb://127.0.0.1:27017"
    cluster = MongoClient(address)
    db = cluster[data_base]
    collection = db[collection]
    result = collection.find()
    a = []
    for i in result:
        a.append(i)
    return str(a)  # Выводим все документы из коллекции


if __name__ == '__main__':
    app.run(debug=True)
